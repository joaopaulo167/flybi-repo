import { createSlice } from '@reduxjs/toolkit'

interface DrawerState {
    open:boolean,
}

const initialState:DrawerState = {
    open: false,
}

const drawerSlice = createSlice({
  name: 'drawer',
  initialState,
  reducers: {
    openDrawer(state, action) {
        return {
            ...state,
            open:  action.payload
        }
    },
    closeDrawer(state, action) {
        return {
            ...state,
            open: action.payload,
        }
    },
  }
})

export const { openDrawer,closeDrawer } = drawerSlice.actions

export default drawerSlice.reducer