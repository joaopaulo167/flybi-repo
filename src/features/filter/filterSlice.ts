import { createSlice } from '@reduxjs/toolkit'

interface FilterState {
    desde:string,
    ate:string,
    visualizacao:string,
    integracao:number,
    integracaoNome:string
}

let yearNow = new Date().getFullYear()
let monthNow:any = new Date().getMonth() + 1
let dayNow:any = new Date().getDate()
if (monthNow<10) {
    monthNow = '0'+ monthNow
}
if(dayNow<10){
    dayNow = '0'+dayNow
}
const initialState:FilterState = {
    visualizacao:'mes',
    desde:yearNow + '-01-01',
    ate:yearNow + '-' + monthNow  + '-' +  dayNow,
    integracao:0,
    integracaoNome:"Todos os Sistemas"
}

const filterSlice = createSlice({
  name: 'filter',
  initialState,
  reducers: {
    setDesde(state,action){
        return{
            ...state,
            desde:action.payload
        }
    },
    setAte(state,action){
        return{
            ...state,
            ate:action.payload
        }
    },
    setVisualizacao(state,action){
        return{
            ...state,
            visualizacao:action.payload
        }
    },
    setIntegracao(state,action){
        return{
            ...state,
            integracao:action.payload
        }
    },
    setIntegracaoNome(state,action){
        return{
            ...state,
            integracaoNome:action.payload
        }
    },
  }
})

export const { setDesde,setAte,setVisualizacao, setIntegracao, setIntegracaoNome } = filterSlice.actions

export default filterSlice.reducer