import { createSlice } from '@reduxjs/toolkit'

interface ChartState {
    statusChart:number[],
    firstChartSeries:any[],
    secondChartSeries:any[],
    thirdChartSeries:any[],
    fourthChartSeries:any[]
}

const initialState:ChartState = {
    statusChart : [0,0,0,0],
    firstChartSeries:[{data: []}],
    secondChartSeries:[{data: []}],
    thirdChartSeries:[{data: []}],
    fourthChartSeries:[{data: []}]
}

const statusChartSlice = createSlice({
  name: 'statusChart',
  initialState,
  reducers: {
    setStatusChartOnScreen(state, action) {
        return {
            ...state,
            statusChart: action.payload
        }
    },
    setFirstChartDataOnScreen(state,action){
        state.firstChartSeries[0].data.push(action.payload)
    },
    setSecondChartDataOnScreen(state,action){
        state.secondChartSeries[0].data.push(action.payload)
    },
    setThirdChartDataOnScreen(state,action){
        state.thirdChartSeries[0].data.push(action.payload)
    },
    setFourthChartDataOnScreen(state,action){
        state.fourthChartSeries[0].data.push(action.payload)
    }
  }
})

export const { setStatusChartOnScreen, 
                setFirstChartDataOnScreen, 
                setSecondChartDataOnScreen, 
                setThirdChartDataOnScreen, 
                setFourthChartDataOnScreen } = statusChartSlice.actions

export default statusChartSlice.reducer