import { createSlice } from '@reduxjs/toolkit'

interface dataDB {
    dataDB:{
      grupos:{
        dias:[],
        meses:[],
        anos:[],
        operadores:[],
        problemas:[],
      },
      media_fechados_por_dia:number,
      total_abertos:number,
      total_atendimentos:number,
      total_fechados:number,
    },
    currentTableModalData:[],
    currentTableModalHeader:[],
    dataDBDia:[],
    dataDBMes:[],
    dataDBAno:[],
    visualizacaoBars:string,
    isLoading:boolean,
    isModalOpen:boolean,
    modalLabel:string,
    boolGotData:boolean
}

const initialState:dataDB = {
    dataDB : {
      grupos:{
        dias:[],
        meses:[],
        anos:[],
        operadores:[],
        problemas:[],
      },
      media_fechados_por_dia:0,
      total_abertos:0,
      total_atendimentos:0,
      total_fechados:0,
    },
    currentTableModalData:[],
    currentTableModalHeader:[],
    dataDBDia:[],
    dataDBMes:[],
    dataDBAno:[],
    visualizacaoBars:'Todos',
    isLoading:true,
    isModalOpen:false,
    modalLabel:'',
    boolGotData:false
}

const dataDBSlice = createSlice({
  name: 'dataDB',
  initialState,
  reducers: {
    setDataDB(state, action) {
        return {
            ...state,
            dataDB: action.payload,
            dataDBDia: action.payload.grupos.dias,
            dataDBMes: action.payload.grupos.meses,
            dataDBAno: action.payload.grupos.anos,
        }
    },
   
    setLoading(state,action){
      return{
        ...state,
        isLoading:action.payload
      }
    },
    setBoolGotData(state,action){
      return{
        ...state,
        boolGotData:action.payload
      }
    },
    setModalOpen(state,action){
      return{
        ...state,
        isModalOpen:action.payload.open,
        modalLabel:action.payload.label
      }
    },
    setTableModalData(state,action){
      return{
        ...state,
        currentTableModalData:action.payload
      }
    },
    setTableModalHeader(state,action){
      return{
        ...state,
        currentTableModalHeader:action.payload
      }
    },
    setVisualizacaoBars(state,action){
      return {
        ...state,
        visualizacaoBars:action.payload
      }
    }
  }
})

export const { setDataDB, setModalOpen, setTableModalData, setTableModalHeader,setVisualizacaoBars } = dataDBSlice.actions

export default dataDBSlice.reducer