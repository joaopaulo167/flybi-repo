//TYPESCRIPT NÂO TEM OS HOOKS USEDISPATCH E USESELECTOR, entao tem que criar personalizado com os tipos declarados 


import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'
import type { RootState, AppDispatch } from '../store'

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector