import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Home  from './screens/home'
import Login  from './screens/login'
import CssBaseline from '@material-ui/core/CssBaseline';
import Suporte from './screens/suporte'
import SuporteSLA from './screens/suporteSLA'

export default function App() {
  return (
    <Router  basename={'/flybi'}>
      <CssBaseline />
      <Switch>
        <Route path="/" exact={true}>
          <Home />
        </Route>
        <Route path="/login" exact>
          <Login />
        </Route>
        <Route path="/suporte" exact>
          <Suporte />
        </Route>
        <Route path="/suporteSLA" exact>
          <SuporteSLA />
        </Route>
      </Switch>
  </Router>
  );
}