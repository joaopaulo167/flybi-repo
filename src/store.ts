import { configureStore } from '@reduxjs/toolkit'

import drawerSlice from './features/drawer/drawerSlice'
import statusChartSlice from './features/statusChart/statusChartSlice'
import dataDBSlice from './features/dataDB/dataDBSlice'
import filterSlice from './features/filter/filterSlice'
const store = configureStore({
    reducer: {
      // Define a top-level state field named `todos`, handled by `todosReducer`
      drawer:drawerSlice,
      statusChart:statusChartSlice,
      dataDB:dataDBSlice,
      filter:filterSlice,
    },

  })

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
  
export default store