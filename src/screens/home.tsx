//TELA HOME
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router-dom";
import { useEffect } from 'react'
import axios from 'axios'

const useStyles = makeStyles({
    root:{
        backgroundColor:'#444444',
        width:'100vw',
        height:'100vh'
    },
    navbarPlaceholder:{
        padding: 10,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 0,
        height: 71,
    },
    card: {
      minWidth: 375,
      maxWidth: 475,
      minHeight: 275,
      padding:0,
      margin:'10%',
      marginTop:50,
      backgroundColor:'#545454',
      justifyContent:'center',
      alignItems:'center'
    },
    button:{
        margin:'10%',
        marginTop:0
    },
    title: {
      fontSize: 34,
      fontWeight:700,
      fontFamily:'Arial',
      color:'#FFF'
    },
  });

export default function Home() {
    const classes = useStyles(); 
    let history = useHistory();

    useEffect(() => {
        let token = localStorage.getItem('TOKEN_USER_FLY');
        axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flybi/verificartoken', { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {

            }).catch(e => {
                history.push('/login')
            })
    }, []);

    return(
        <>
            <div className={classes.root}>
                <div className={classes.navbarPlaceholder}>
                    <h1 style={{padding:0,margin:0,color:'#FFF'}}>Selecionar BI</h1>
                </div>
                <div >
                    <Button className={classes.button}>
                        <Card className={classes.card} onClick={()=>{history.push('/suporte')}}>
                        
                            <p className={classes.title} >
                                BI Geral do Suporte
                            </p>
                        </Card>
                    </Button>
                   
             
                </div>

            </div>
        </>
    );
}