//TELA DE SUPORTE
import Navbar from '../components/Navbar/Navbar'
import Drawer from '../components/Drawer/Drawer'
import HeaderCharts from '../components/Charts/HeaderCharts/HeaderChart'
import BodyCharts from '../components/Charts/bodyCharts/bodyCharts'
import axios from 'axios'
import { useEffect } from 'react'
import { useAppDispatch as useDispatch, useAppSelector as useSelector } from '../hooks/hooks'
import ReactLoading from 'react-loading';
import { useHistory } from "react-router-dom";

export default function Home() {
    let history = useHistory();
    const filterState = useSelector(state => state.filter)
    const dataState = useSelector(state => state.dataDB)
    const dispatch = useDispatch()

    useEffect(() => {
        let token = localStorage.getItem('TOKEN_USER_FLY');
        axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flyBI/verificartoken', { headers: { "Authorization": `Bearer ${token}` } }).then(response => {
            dispatch({ type: 'dataDB/setLoading', payload: true })
            axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flyBI/getPeriod?start=' + filterState.desde + '&end=' + filterState.ate)
                .then((res) => {
                    console.log(res)
                    let data = JSON.parse(JSON.stringify(res.data))
                    if (data.data.atual != undefined) {
                        if (data.data.atual.grupos) {
                            dispatch({ type: 'dataDB/setDataDB', payload: data.data.atual })
                            dispatch({
                                type: 'statusChart/setStatusChartOnScreen', payload: [new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(data.data.atual.total_atendimentos),
                                new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(data.data.atual.total_fechados),
                                new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(data.data.atual.media_fechados_por_dia),
                                new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(data.data.atual.total_abertos)]
                            })
                            if(data.data.passado.grupos){
                            data.data.passado.grupos.meses.map((element: any) => {
                                dispatch({ type: 'statusChart/setFirstChartDataOnScreen', payload: element.total_aberto })
                                dispatch({ type: 'statusChart/setSecondChartDataOnScreen', payload: element.total_fechado })
                                dispatch({ type: 'statusChart/setThirdChartDataOnScreen', payload: element.media })
                            })
                            }
                        }
                    }
                    dispatch({ type: 'dataDB/setLoading', payload: false })
                })
        }).catch(e => {
            history.push('/login')
            console.log(e);
        })
    }, [])

    return (
        <>  {

            dataState.isLoading === false ? (
                <div style={{ backgroundColor: '#444444', width: '100vw', height: '100vh' }}>
                    <Drawer />
                    <Navbar />
                    <HeaderCharts />
                    <BodyCharts />
                </div>
            ) :
                (
                    <div style={{ width: '100vw', height: '100vh', display: 'flex', margin: 0, backgroundColor: '#545454', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                        <ReactLoading type={'cylon'} color={'black'} width={200} height={200} />
                        <h1>Carregando, aguarde alguns instantes...</h1>
                    </div>
                )
        }
        </>
    );
}