import { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import logo from '../media/logo.png';
import axios from 'axios';
import { useAppDispatch as useDispatch, useAppSelector as useSelector } from '../hooks/hooks';
import ReactLoading from 'react-loading';

const useStyles = makeStyles({
    messageStyle: {
        position: 'fixed',
        margin: 'auto',
        width: '100%',
        textAlign: 'center',
        marginBottom: '10px'
    },
    overlayShow: {
        position: 'fixed',
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.5)',
        zIndex: 2
    },
    overlayHidden: {
        display: 'none'
    },
    overlayImg: {
        width: '15%',
        marginTop: '20%',
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    divContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100vh'
    },
    formStyle: {
        width: '400px',
        background: '#191970',
        padding: '20px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        border: '1px solid #ddd',
        borderRadius: '40px'
    },
    formImg: {
        width: '100px',
        margin: '10px 0 40px'
    },
    formP: {
        color: '#ff3333',
        marginBottom: '15px',
        border: '1px solid #ff3333',
        padding: '10px',
        width: '100%',
        textAlign: 'center'
    },
    formInput: {
        flex: 'a',
        height: '46px',
        marginBottom: '15px',
        padding: '0 20px',
        color: '#777',
        fontSize: '20px',
        width: '80%',
        border: '1px solid #ddd',
        borderRadius: '10px',
        "&::placeholder": {
            color: '#999'
        }
    },
    formButton: {
        color: '#fff',
        fontSize: '20px',
        background: '#FF7F50',
        height: '56px',
        border: 0,
        borderRadius: '10px',
        width: '90%'
    },
    formA: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#999',
        textDecoration: 'none'
    }
});

export default function Login() {
    const classes = useStyles();
    let history = useHistory();
    const [tela, setTela] = useState('login');
    const [mensagem, setMensagem] = useState('');
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [novaSenha, setNovaSenha] = useState('');
    const [novaSenhaConfirma, setNovaSenhaConfirma] = useState('');
    const dispatch = useDispatch();
    const dataState = useSelector(state => state.dataDB)

    //Passando array vazio, funciona como componentdidmount
    useEffect(() => {
        let token = localStorage.getItem('TOKEN_USER_FLY');
        if (token) {
            axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flybi/verificartoken', { headers: { "Authorization": `Bearer ${token}` } }).then(response => {
                history.push('/')
            }).catch(e => {

            })
        }
        const hash = new URL(window.location.href).searchParams.get('criar_senha');
        if (hash !== null) {
            setTela('cadastro');
        }
        dispatch({ type: 'dataDB/setLoading', payload: false })
    }, [])

    const handleSubmit = (event: any) => {
        event.preventDefault();
        dispatch({ type: 'dataDB/setLoading', payload: true })
        axios.post(process.env.REACT_APP_API_HOST + 'api/v1/autenticar/login', { "login": login, "password": password, "sistema": 1 }).then(response => {
            console.log(response)
            setMensagem(response.data.data.mensagem);
            if (response.data.data.auth === true) {
                var token = response.data.data.token;
                localStorage.setItem('TOKEN_USER_FLY', token);
                let obj: {
                    token: string,
                    nome: string,
                    permissoes: Array<number>,
                    sistemas: Array<number>
                } = { token: '', nome: '', permissoes: [], sistemas: [] };
                let sistemas = [];
                let permissoes_value = [];
                let permissoes = response.data.data.permissoes;
                for (var i = 0; i < permissoes.length; i++) {
                    sistemas.push(permissoes[i].sistema_id)
                    permissoes_value.push(permissoes[i].permissao_id)
                }
                obj.token = token;
                obj.nome = response.data.data.nome;
                obj.permissoes = permissoes_value;
                obj.sistemas = sistemas;
                dispatch({ type: 'dataDB/setLoading', payload: false })
                history.push('/')
            } else {
                dispatch({ type: 'dataDB/setLoading', payload: false })
            }
        }).catch(e => {
            dispatch({ type: 'dataDB/setLoading', payload: false })
            setMensagem("Não foi possível entrar no sistema.")
        });
    }

    const handleCreate = (event: any) => {
        event.preventDefault();
        if (novaSenha.length > 0) {
            if (novaSenha !== novaSenhaConfirma) {
                setMensagem('Senhas não são iguais.');
            } else {
                dispatch({ type: 'dataDB/setLoading', payload: true })
                const hash = new URL(window.location.href).searchParams.get('criar_senha');
                axios.post(process.env.REACT_APP_API_HOST + 'api/v1/autenticar/cadastrar_senha', { "hash": hash, "senha": novaSenha, "sistema": 1 }).then(response => {
                    if (response.data.data.success === true) {
                        setMensagem(response.data.data.mensagem);
                        setTela('login');
                    } else {
                        setMensagem(response.data.data.mensagem);
                    }
                    dispatch({ type: 'dataDB/setLoading', payload: false })
                }).catch(e => {
                    dispatch({ type: 'dataDB/setLoading', payload: false })
                    setMensagem("Ocorreu um erro")
                });
            }
        }
    }

    const changeLogin = (event: any) => {
        setLogin(event.target.value);
    }

    const changeSenha = (event: any) => {
        setPassword(event.target.value);
    }

    const changeNovaSenha = (event: any) => {
        setNovaSenha(event.target.value);
    }

    const changeNovaSenhaConfirma = (event: any) => {
        setNovaSenhaConfirma(event.target.value);
    }

    const esqueciMinhaSenha = (event: any) => {
        event.preventDefault();
        dispatch({ type: 'dataDB/setLoading', payload: true })
        if (login) {
            axios.post(process.env.REACT_APP_API_HOST + 'api/v1/autenticar/nova_senha', { "login": login, "sistema": 1 }).then(response => {
                setMensagem(response.data.data.mensagem);
                dispatch({ type: 'dataDB/setLoading', payload: false })
            });
        } else {
            dispatch({ type: 'dataDB/setLoading', payload: false })
            setMensagem("Informe um e-mail.");
        }
    }
    if (tela === "login") {
        return (
            <>
                {dataState.isLoading === false ? (
                    <>
                        <p className={classes.messageStyle}>{mensagem}</p>
                        <div className={classes.divContainer}>
                            <form className={classes.formStyle} onSubmit={(e: any) => handleSubmit(e)}>
                                <img src={logo} className={classes.formImg} alt="Flybyte Logo" />
                                <input className={classes.formInput} type="text" placeholder="Login" value={login} onChange={changeLogin} />
                                <input className={classes.formInput} type="password" placeholder="Senha" value={password} onChange={changeSenha} />
                                <a className={classes.formA} style={{ marginBottom: '10px' }} onClick={esqueciMinhaSenha} href="">Esqueci minha senha</a>
                                <button className={classes.formButton} style={{ cursor: 'pointer' }} type="submit">Entrar</button>
                            </form>
                        </div>
                    </>
                ) :
                    (
                        <div style={{ width: '100vw', height: '100vh', display: 'flex', margin: 0, backgroundColor: '#545454', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                            <ReactLoading type={'cylon'} color={'black'} width={200} height={200} />
                            <h1>Carregando, aguarde alguns instantes...</h1>
                        </div>
                    )
                }
            </>
        )
    } else {
        return (
            <>
                {dataState.isLoading === false ? (
                    <>
                        <p className={classes.messageStyle}>{mensagem}</p>
                        <div className={classes.divContainer}>
                            <form className={classes.formStyle} onSubmit={(e: any) => handleCreate(e)}>
                                <img src={logo} className={classes.formImg} alt="Flybyte Logo" />
                                <input className={classes.formInput} type="password" placeholder="Digite sua nova senha" value={novaSenha} onChange={changeNovaSenha} />
                                <input className={classes.formInput} type="password" placeholder="Confirme sua nova senha" value={novaSenhaConfirma} onChange={changeNovaSenhaConfirma} />
                                <button className={classes.formButton} style={{ cursor: 'pointer' }} type="submit">Criar Senha</button>
                            </form>
                        </div>
                    </>
                ) :
                    (
                        <div style={{ width: '100vw', height: '100vh', display: 'flex', margin: 0, backgroundColor: '#545454', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                            <ReactLoading type={'cylon'} color={'black'} width={200} height={200} />
                            <h1>Carregando, aguarde alguns instantes...</h1>
                        </div>
                    )
                }
            </>
        )
    }
}