import Navbar from '../components/Navbar/NavbarSLA'
import Drawer from '../components/Drawer/Drawer'
import HeaderCharts from '../components/Charts/HeaderCharts/HeaderChartSLA'
import BodyCharts from '../components/Charts/bodyCharts/bodyCharts'
import axios from 'axios'
import ReactLoading from 'react-loading';
import { useEffect } from 'react';
import { useHistory } from "react-router-dom";

export default function SuporteSLA() {
    let history = useHistory();

    useEffect(() => {
        let token = localStorage.getItem('TOKEN_USER_FLY');
        axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flybi/verificartoken', { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {

            }).catch(e => {
                history.push('/login')
            })
    }, []);

    return(
        <>
            <div style ={{backgroundColor:'#444444', width:'100vw',height:'100vh'}}>
                <Drawer/>
                <Navbar/>
                <HeaderCharts/>
            </div>  
        </>    
    );
}