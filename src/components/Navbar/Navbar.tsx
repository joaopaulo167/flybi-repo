
import React from 'react'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import { makeStyles} from '@material-ui/core/styles';
import {useAppSelector as useSelector, useAppDispatch as useDispatch} from '../../hooks/hooks'
import { ChevronLeft } from '@material-ui/icons';

import { useHistory } from "react-router-dom";
const drawerWidth = 230;
const useStyles = makeStyles((theme?: any) => ({
    appBar: {
      padding: 10,
      marginLeft: 20,
      marginRight: 20,
      height: 71,
      transition: theme.transitions.create(['margin', 'width'], {
         easing: theme.transitions.easing.easeInOut,
        duration: theme.transitions.duration.leavingScreen,
      }),
      display:'flex',
      flexDirection:'row',
      flexGrow:1
    },
    appBarShift: {
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: drawerWidth,
      display:'flex',
      flexDirection:'row',
      flexGrow:1
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    hide: {
      display: 'none',
    }
  }));



export default function Navbar(){
    const dispatch = useDispatch();
    const classes = useStyles();
    const drawerState = useSelector(state => state.drawer)
    const statusChartState = useSelector(state =>state.statusChart)
    const filterState = useSelector(state =>state.filter)

    let history = useHistory();
    const handleDrawerOpen = () => {
        dispatch({type:'drawer/openDrawer',payload:true})
      };

    const formatDataView = (data:string) =>{
      let dataValues = data.split('-');
      let dia = dataValues[2];
      let mes = dataValues[1];
      let ano = dataValues[0];
      return dia+'/'+mes+'/'+ano;
    }

    return(
        <div id='nav-bar' className={clsx(classes.appBar , { [classes.appBarShift]: drawerState.open, })}>
            <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, drawerState.open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <IconButton onClick={()=>{history.goBack()}}>
                <ChevronLeft />
            </IconButton>
            <p style={{fontFamily:'Roboto',fontSize:'30px',fontWeight:600,marginTop:5,color:'#FFF',width:'500px'}}>Visão geral de suporte </p>
            <p style={{position:'absolute',marginTop:55,marginLeft:50,color:'#FFF'}}> {formatDataView(filterState.desde)} - {formatDataView(filterState.ate)}</p>
            {
              statusChartState.statusChart[3] === 0 ? (
                null
              ): (
                <>
                  {/* <IconButton style={{right:0,padding:20,backgroundColor:'rgba(100, 110, 251, 1)',borderRadius:30,marginLeft:'55%'}} onClick={()=>{window.location.href='/suporteSLA'}}>
                      <p>Explorar SLA</p>
                      <ChevronRight />
                  </IconButton> */}
                </>
              )
            }
            
        </div>
    )
}

