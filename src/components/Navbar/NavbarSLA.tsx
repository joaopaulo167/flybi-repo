import React from 'react'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import { makeStyles} from '@material-ui/core/styles';
import {useAppSelector as useSelector, useAppDispatch as useDispatch} from '../../hooks/hooks'
import { ChevronLeft } from '@material-ui/icons';

const drawerWidth = 200;
const useStyles = makeStyles((theme?: any) => ({
    appBar: {
      padding: 10,
      marginLeft: 20,
      marginRight: 20,
      height: 71,
      transition: theme.transitions.create(['margin', 'width'], {
         easing: theme.transitions.easing.easeInOut,
        duration: theme.transitions.duration.leavingScreen,
      }),
      display:'flex',
      flexDirection:'row',
      flexGrow:1
    },
    appBarShift: {
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: drawerWidth,
      display:'flex',
      flexDirection:'row',
      flexGrow:1
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    hide: {
      display: 'none',
    }
  }));



export default function Navbar(){
    const dispatch = useDispatch();
    const classes = useStyles();
    const drawerState = useSelector(state => state.drawer)

    const handleDrawerOpen = () => {
        dispatch({type:'drawer/openDrawer',payload:true})
      };

    return(
        <div id='nav-bar' className={clsx(classes.appBar , { [classes.appBarShift]: drawerState.open, })}>
            <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, drawerState.open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <IconButton>
                <ChevronLeft onClick={()=>{window.location.href='/home'}}/>
            </IconButton>
            <p style={{fontFamily:'Roboto',fontSize:'30px',fontWeight:600,marginTop:5,color:'#FFF',width:'500px'}}>SLAs - Em aberto</p>
            {/* <div style={{right:0,width:'100%',display:'flex',flexDirection:'row',justifyContent:'end',alignItems:'end'}}>
              <IconButton onClick={()=>{window.location.href='/SLAaberto'}} style={{backgroundColor:'rgba(0, 143, 251, 1)',padding:20,borderRadius:30,margin:5}}>
                  <p>Em Aberto</p>
              </IconButton>
              <IconButton  onClick={()=>{window.location.href='/SLAtotalAtendido'}} style={{backgroundColor:'rgba(0, 143, 251, 1)',padding:20,borderRadius:30,margin:5}}>
                  <p>Total Atendido</p>
              </IconButton>
              <IconButton  onClick={()=>{window.location.href='/SLAAtendido'}} style={{backgroundColor:'rgba(0, 143, 251, 1)',padding:20,borderRadius:30,margin:5}}>
                  <p>SLA Atendido</p>
              </IconButton>
              <IconButton  onClick={()=>{window.location.href='/SLAExtrapolado'}} style={{backgroundColor:'rgba(0, 143, 251, 1)',padding:20,borderRadius:30,margin:5}}>
                  <p>SLA Extrapolado</p>
              </IconButton>
            </div> */}
           
        </div>
    )
}

