//componente responsável pelos graficos nos status
import Chart from 'react-apexcharts'
import { makeStyles} from '@material-ui/core/styles';
import AddIcCallRoundedIcon from '@material-ui/icons/AddIcCallRounded';
import { useState } from 'react';
import clsx from 'clsx';
import {useAppSelector as useSelector} from '../../../hooks/hooks'
import { useEffect } from 'react';
const drawerWidth = 200;

const useStyles = makeStyles((theme) => ({
    card: {
      backgroundColor: '#545454',
      padding: 30,
      borderRadius: 30,
      heigth: '100%', 
      width: '22%',
      display:'flex',
      flexDirection:'row',
      alignItems:'center',
      minWidth:'280px'
    },
    content: {
        height: 160,
        maxHeight:400, 
        marginTop: 0 ,
        display:'flex',
        flexWrap:'wrap',
        backgroundColor: '#444444',
        flexGrow: 1,
        transition: theme.transitions.create('margin', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: 0 ,
        alignItems: 'center' ,
    },
    contentShift: {
        height: 160, 
        display:'flex',
        flexWrap:'wrap',
        marginTop: 0 ,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: drawerWidth,
    },
    cardIcon:{
        border:'1px solid black',
        borderRadius:100,
        paddingTop:15,
        paddingBottom:10,
        paddingLeft:12,
        paddingRight:15,
    },
    charts:{
        height: 108, 
        width: '100%', 
        display: 'flex', 
        flexDirection: 'row', 
        justifyContent: 'space-evenly' 
    },
    chartTitle:{
        width:'200px',
        position:'absolute', 
        marginLeft:20,
        paddingTop:20,
        color:'#FFF',
        fontFamily:'Roboto',
        fontWeight:400
    },
    chartValue:{
        position:'absolute', 
        marginLeft:20,
        paddingTop:35,
        fontSize:'24px',
        color:'#FFF',
        fontFamily:'Roboto',
        fontWeight:900
    },
    chartCard:{
        marginTop:60,
        marginLeft:40
    },
}));

interface ChartLine{
    options:Object,
    tooltip:Object
}
 //grafico linha para os status
const stateLineTotal:ChartLine = {
    tooltip: {
        enabled: true,
    },
    options: {
        chart: {
          type: 'line',
          zoom: {
              enabled: false
          },
          toolbar: {
              show: false,
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'smooth'
        },
        title: {
          show:false,
          align: 'left',
          style: {
              color: '#FFF',
              fontSize: 18,
              fontFamily: 'Roboto',
              fontWeight: 400
          }
        },
        grid: {
          show:false,
          row: {
              colors: ['transparent'], // takes an array which will be repeated on columns
              opacity: 0.1
          },
        },
        xaxis: {
          labels: {
              show:false,
          },
          axisBorder: {
              show: false
          },
          axisTicks: {
              show: false,
          }
        },
        yaxis: {
          labels: {
              show:false,
          },
        }
    },
};

export default function HeaderCharts(){
    const classes = useStyles();
    const drawerState = useSelector(state => state.drawer)
    const dataState = useSelector(state => state.dataDB)
    const [totalAtendimentos,setTotalAtendimentos] = useState(0)
    const [totalFechados,setTotalFechados] = useState(0)
    const [mediaDiaria,setMediaDiaria] = useState(0)
    const [totalAbertos,setTotalAbertos] = useState(0)
    const [totalAtendimentosSeries,setTotalAtendimentosSeries] = useState([{data: []}])
    const [totalFechadosSeries,setTotalFechadosSeries] = useState( [{data: []}])
    const [mediaDiariaSeries,setMediaDiariaSeries] = useState([{data: []}])
    const [totalAbertosSeries,setTotalAbertosSeries] = useState([{data: []}])
    const {statusChart,firstChartSeries,secondChartSeries,thirdChartSeries,fourthChartSeries} = useSelector(state => state.statusChart)
    
    useEffect(()=>{
      setTotalAtendimentos(statusChart[0])
      setTotalFechados(statusChart[1])
      setMediaDiaria(statusChart[2])
      setTotalAbertos(statusChart[3])
      setTotalAtendimentosSeries(firstChartSeries)
      setTotalFechadosSeries(secondChartSeries)
      setMediaDiariaSeries(thirdChartSeries)
      setTotalAbertosSeries(fourthChartSeries)
    },[])

    return(
        <>
         <div id='body' className={clsx(classes.content, { [classes.contentShift]: drawerState.open, })}>
             <div id='status-charts' className={classes.charts}>
               {
                 [
                   {color:'rgba(100, 110, 251, 1)',title:'Total Atendimentos Abertos',series:totalAtendimentosSeries,value:totalAtendimentos},
                   {color:'rgba(110, 200, 50, 1)',title:'Total Atendimentos Fechados',series:totalFechadosSeries,value:totalFechados},
                   {color:'rgba(200, 50, 50, 1)',title:'Atendimentos Pendentes',series:totalAbertosSeries,value:totalAbertos},
                   {color:'rgba(200, 110, 50, 1)',title:'Média Diária Fechados',series:mediaDiariaSeries,value:mediaDiaria},
                  ].map((element)=>{
                    return(
                      <div className={classes.card} >
                        <div style={{backgroundColor:element.color}} className={classes.cardIcon}>
                          <AddIcCallRoundedIcon fontSize='large'/>
                        </div>
                        <div>
                          <p className={classes.chartTitle}>{element.title}</p>
                          <p className={classes.chartValue}>{element.value}</p>
                          <Chart  options={stateLineTotal.options} series={element.series} type="line" height={70} width={'85%'} className={classes.chartCard} />
                        </div>
                    </div>
                    )
                 })
               }
            </div>
            </div>
        </>
    )
}