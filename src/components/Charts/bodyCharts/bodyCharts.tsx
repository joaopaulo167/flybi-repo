

//Component responsável por gerar o corpo da tela com os 5 charts
import Chart from 'react-apexcharts'
import clsx from 'clsx';
import { makeStyles} from '@material-ui/core/styles';
import {useAppSelector as useSelector,useAppDispatch as useDispatch} from '../../../hooks/hooks'
import { useState } from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { useEffect } from 'react';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { DataGrid, GridToolbar } from '@material-ui/data-grid';
import axios from 'axios';

const drawerWidth = 230;
const useStyles = makeStyles((theme:any) => ({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4),
      height:'80%',
      width:'80%'
    },
    card: {
        WebkitScrollSnapType:'#444444',
        backgroundColor: '#545454',
        padding: 20,
        borderRadius: 30,
        margin: 10,
        width:'35%',
        overflow:'auto',
        maxHeight:360
      },
      cardRadial: {
        backgroundColor: '#545454',
          width:'35%',
      },
      cardSecondCard: {
        backgroundColor: '#545454',
        borderRadius: 30,
        margin: 10,
        padding:10,
        width:'60%',
        display:'flex',
        flexDirection:'row',
        maxHeight:360
      },
      cardMainBar: {
        backgroundColor: '#545454',
        padding: 20,
        width:'60%',
        borderRadius: 30,
        margin: 10
      },
      content: {
          marginTop: 0 ,
          display:'flex',
          backgroundColor: '#444444',
          flexWrap:'wrap',
          transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
          }),
          marginLeft: 0 ,
          alignItems: 'center' ,
      },
      contentShift: {
        marginTop: 0 ,
        flexWrap:'wrap',
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: drawerWidth,
      },
}));

//declara tipos para os charts
interface ChartType{
    series:any[],
    options:Object,
    tooltip:Object
}

interface seriesInterface{
  name:string,
  data:any[]
}


export default function BodyCharts(){
    
    //variavel para os menus
    const [anchorEl, setAnchorEl] = useState(null);
    const handleClick = (event:any) => {
      setAnchorEl(event.currentTarget);
    };
    const dispatch = useDispatch()
    const handleClose = (op:string) => {
      if (op) {
        dispatch({type:'dataDB/setVisualizacaoBars',payload:op})
      }
      setAnchorEl(null);
    };

    const formatDataView = (data:string) =>{
      let dataValues = data.split('-');
      let dia = dataValues[2];
      let mes = dataValues[1];
      let ano = dataValues[0];
      return dia+'/'+mes+'/'+ano;
    }
   
    const classes = useStyles();
    //altura dos charts
    const [ heightBar1, setHeightBar1] = useState(2000)
    const [ heightBar2, setHeightBar2] = useState(2000)
    const drawerState = useSelector(state => state.drawer)
    const dataState = useSelector(state => state.dataDB)
    const filterState = useSelector(state => state.filter)
    //calculo para a porcentagem do chart radialbar
    let porcBarra = dataState.dataDB.total_abertos /dataState.dataDB.total_fechados *100
    if(porcBarra > 0 && porcBarra < 1) porcBarra = 1
    const stateCategories:any[] = []
    const stateBarCategories:any[] = []
    let stateBarSeries:any[] = []
    const stateBar2Categories:any[] = []
    let stateBar2Series:number[] = []
    const stateLineCategories:any[] = []
    const stateSeries:seriesInterface[] = [
      {name:'Novos Chamados',data:[]},
      {name:'Chamados Fechados',data:[]},
    ]
    const stateLineSeries:seriesInterface[] = [
      {name:'Chamados em aberto',data:[]},
    ]
    //cuida do abrir do modal com as tabelas
    const handleOpenModal = (typeOpening:string,value:any) => {    
      
      dispatch({type:'dataDB/setLoading',payload:true})
      if (typeOpening === 'period') {
        
        dispatch({type:'dataDB/setTableModalHeader',payload:[
          { field: 'id',  hide:true },
          { field: 'idMK', headerName: 'ID', width: 90 },
          {
            field: 'problema',
            headerName: 'Problema',
            width: 200,
            editable: true,
          },
          {
            field: 'solucao',
            headerName: 'Solução',
            width: 200,
            editable: true,
          },
          {
            field: 'operador',
            headerName: 'Operador',
            width: 200,
            editable: true,
          },
          {
            field: 'cliente',
            headerName: '(id) - Cliente ',
            width: 300,
            editable: true,
          },
          {
            field: 'duracao',
            headerName: 'Duração(duração/dia)',
            description: 'Duração igual a diferença entre a data de abertura e de fechamento',
            sortable: false,
            width: 200,
          },
          {
            field: 'abertura',
            headerName: 'Data Abertura',
            sortable: false,
            width: 200,
          },
          {
            field: 'fechamento',
            headerName: 'Data Fechamento',
            sortable: false,
            width: 200,
          },
        ]})
        let splited = value.split('/')
        if (splited.length === 1) {
          axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flyBI/getatendimento?ano='+value)
          .then((res:any)=>{
            let data = JSON.parse(JSON.stringify(res.data))
            let auxrows:any[] = []
            data.data.map((element:any)=>{
              let encerramento:any = !(element.data_hora_encerramento == null) ? element.data_hora_encerramento.split('T')[0] : '-'
              let abertura:any = !(element.data_hora_abertura == null) ? element.data_hora_abertura.split('T')[0] : '-'
              auxrows.push({
                id:element.id,
                idMK:element.id_externo, 
                problema:element.problema,
                solucao:element.solucao,
                operador:element.operador.split(' - ')[1],
                cliente:element.cliente_id+' - '+element.cliente,
                duracao:(element.tempo_encerra / (60*60*24)).toFixed(0) + ' dias',
                abertura:formatDataView(abertura),
                fechamento:formatDataView(encerramento)
              })
            })
            dispatch({type:'dataDB/setTableModalData',payload:auxrows})
            dispatch({type:'dataDB/setLoading',payload:false})
            dispatch({type:'dataDB/setModalOpen',payload:{open:true,label:value}})
          })
          .catch((error:string)=>{
            dispatch({type:'dataDB/setModalOpen',payload:{open:false,label:''}})
            dispatch({type:'dataDB/setLoading',payload:false})
            console.log(error)
            alert(error)
          })
        }else if(splited.length === 2){
          axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flyBI/getatendimento?mes=' + splited[0] + '&ano=' + splited[1])
          .then((res:any)=>{
            let data = JSON.parse(JSON.stringify(res.data))
            let auxrows:any[] = []
            data.data.map((element:any)=>{
              let encerramento:any = !(element.data_hora_encerramento == null) ? element.data_hora_encerramento.split('T')[0] : '-'
              let abertura:any = !(element.data_hora_abertura == null) ? element.data_hora_abertura.split('T')[0] : '-'
              auxrows.push({
                id:element.id,
                idMK:element.id_externo, 
                problema:element.problema,
                solucao:element.solucao,
                operador:element.operador.split(' - ')[1],
                cliente:element.cliente_id+' - '+element.cliente,
                duracao:(element.tempo_encerra / (60*60*24)).toFixed(0) + ' dias',
                abertura:formatDataView(abertura),
                fechamento:formatDataView(encerramento)
              })
            })
            dispatch({type:'dataDB/setTableModalData',payload:auxrows})
            dispatch({type:'dataDB/setLoading',payload:false})
            dispatch({type:'dataDB/setModalOpen',payload:{open:true,label:value}})
          })
          .catch((error:string)=>{
            dispatch({type:'dataDB/setModalOpen',payload:{open:false,label:''}})
            dispatch({type:'dataDB/setLoading',payload:false})
            console.log(error)
            alert(error)
          })
        }else if(splited.length === 3){
          axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flyBI/getatendimento?dia=' + splited[0] + '&mes=' + splited[1] + '&ano='+ splited[2])
          .then((res:any)=>{
            let data = JSON.parse(JSON.stringify(res.data))
            let auxrows:any[] = []
            data.data.map((element:any)=>{
              
              let encerramento:any = !(element.data_hora_encerramento == null) ? element.data_hora_encerramento.split('T')[0] : '-'
              let abertura:any = !(element.data_hora_abertura == null) ? element.data_hora_abertura.split('T')[0] : '-'
              auxrows.push({
                id:element.id,
                idMK:element.id_externo, 
                problema:element.problema,
                solucao:element.solucao,
                operador:element.operador.split(' - ')[1],
                cliente:element.cliente_id+' - '+element.cliente,
                duracao:(element.tempo_encerra / (60*60*24)).toFixed(0) + ' dias',
                abertura:formatDataView(abertura),
                fechamento:formatDataView(encerramento)
              })
            })
            dispatch({type:'dataDB/setTableModalData',payload:auxrows})
            dispatch({type:'dataDB/setLoading',payload:false})
            dispatch({type:'dataDB/setModalOpen',payload:{open:true,label:value}})
          })
          .catch((error:string)=>{
            dispatch({type:'dataDB/setModalOpen',payload:{open:false,label:''}})
            dispatch({type:'dataDB/setLoading',payload:false})
            console.log(error)
            alert(error)
          })
        }
      }else if(typeOpening === "operador"){
        dispatch({type:'dataDB/setTableModalHeader',payload:[
          { field: 'id', headerName: 'ID', width: 90 },
          {
            field: 'problema',
            headerName: 'Problema',
            width: 200,
            editable: true,
          },
          {
            field: 'solucao',
            headerName: 'Solução',
            width: 200,
            editable: true,
          },
          {
            field: 'cliente',
            headerName: '(id) - Cliente ',
            width: 300,
            editable: true,
          },
          {
            field: 'duracao',
            headerName: 'Duração(duração/dia)',
            description: 'Duração igual a diferença entre a data de abertura e de fechamento',
            sortable: false,
            width: 200,
          },
          {
            field: 'abertura',
            headerName: 'Data Abertura',
            sortable: false,
            width: 200,
          },
          {
            field: 'fechamento',
            headerName: 'Data Fechamento',
            sortable: false,
            width: 200,
          },
        ]})
        
        axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flyBI/getatendimento?start='+filterState.desde + '&end=' + filterState.ate + '&operador='+value.split(' - ')[0])
        .then((res:any)=>{
          let data = JSON.parse(JSON.stringify(res.data))
          let auxrows:any[] = []
          data.data.map((element:any)=>{
            if (element.problema.split(':')[0]==dataState.visualizacaoBars || dataState.visualizacaoBars == 'Todos') {
              let encerramento:any = !(element.data_hora_encerramento == null) ? element.data_hora_encerramento.split('T')[0] : '-'
              let abertura:any = !(element.data_hora_abertura == null) ? element.data_hora_abertura.split('T')[0] : '-'
              auxrows.push({
                id:element.id,
                idMK:element.id_externo, 
                problema:element.problema,
                solucao:element.solucao,
                cliente:element.cliente_id+' - '+element.cliente,
                duracao:(element.tempo_encerra / (60*60*24)).toFixed(0) + ' dias',
                abertura:formatDataView(abertura),
                fechamento:formatDataView(encerramento)
              })
            }
          })
          
          dispatch({type:'dataDB/setTableModalData',payload:auxrows})
          dispatch({type:'dataDB/setLoading',payload:false})
          dispatch({type:'dataDB/setModalOpen',payload:{open:true,label:value}})
        })
        .catch((error:string)=>{
          dispatch({type:'dataDB/setModalOpen',payload:{open:false,label:''}})
          dispatch({type:'dataDB/setLoading',payload:false})
          console.log(error)
          alert(error)
        })
      }else if(typeOpening === "problema"){
        dispatch({type:'dataDB/setTableModalHeader',payload:[
          { field: 'id', headerName: 'ID', width: 90 },
          {
            field: 'operador',
            headerName: 'Operdador',
            width: 200,
            editable: true,
          },
          {
            field: 'solucao',
            headerName: 'Solução',
            width: 200,
            editable: true,
          },
          {
            field: 'cliente',
            headerName: '(id) - Cliente ',
            width: 300,
            editable: true,
          },
          {
            field: 'duracao',
            headerName: 'Duração(duração/dia)',
            description: 'Duração igual a diferença entre a data de abertura e de fechamento',
            sortable: false,
            width: 200,
          },
          {
            field: 'abertura',
            headerName: 'Data Abertura',
            sortable: false,
            width: 200,
          },
          {
            field: 'fechamento',
            headerName: 'Data Fechamento',
            sortable: false,
            width: 200,
          },
        ]})
        axios.get(process.env.REACT_APP_API_HOST + 'api/v1/flyBI/getatendimento?start=' + filterState.desde +'&end='+ filterState.ate +'&problema='+value.split('/')[0])
        .then((res:any)=>{
          
          let data = JSON.parse(JSON.stringify(res.data))
          let auxrows:any[] = []
          data.data.map((element:any)=>{
            let encerramento:any = !(element.data_hora_encerramento == null) ? element.data_hora_encerramento.split('T')[0] : '-'
            let abertura:any = !(element.data_hora_abertura == null) ? element.data_hora_abertura.split('T')[0] : '-'
            auxrows.push({
              id:element.id,
              idMK:element.id_externo, 
              operador:element.operador.split(' - ')[1],
              solucao:element.solucao,
              cliente:element.cliente_id+' - '+element.cliente,
              duracao:(element.tempo_encerra / (60*60*24)).toFixed(0) + ' dias',
              abertura:formatDataView(abertura),
              fechamento:formatDataView(encerramento)
            })
          })
          dispatch({type:'dataDB/setTableModalData',payload:auxrows})
          dispatch({type:'dataDB/setLoading',payload:false})
          dispatch({type:'dataDB/setModalOpen',payload:{open:true,label:value}})
        })
        .catch((error:string)=>{
          dispatch({type:'dataDB/setModalOpen',payload:{open:false,label:''}})
          dispatch({type:'dataDB/setLoading',payload:false})
          alert(error)
        })
      }
    };
  
    const handleCloseModal = () => {
      dispatch({type:'dataDB/setModalOpen',payload:{open:false,label:''}})
    };

    //switch do filtro de visualização no componente Drawer, pois conforme muda a visualização tem que ser agrupado diferente os arrays para os charts
    switch (filterState.visualizacao) {
      case 'mes':
        dataState.dataDBMes.map((element:any)=>{  
          stateCategories.push(element.descricao)
          stateLineCategories.push(element.descricao)
          stateSeries[0].data.push(element.total_aberto)
          stateSeries[1].data.push(element.total_fechado)
          stateLineSeries[0].data.push(element.em_aberto)
          if (dataState.dataDBMes.length < 2) {
            stateLineSeries[0].data.push(element.em_aberto)
          }
        })
        break;
      case 'semanas':
          break; 
      case 'dia':
        dataState.dataDBDia.map((element:any)=>{
          stateLineCategories.push(element.descricao)
          stateCategories.push(element.descricao)
          stateSeries[0].data.push(element.total_aberto)
          stateSeries[1].data.push(element.total_fechado)
          stateLineSeries[0].data.push(element.em_aberto)
        })
          break;
      case 'ano':
        dataState.dataDBAno.map((element:any)=>{
          stateLineCategories.push(element.descricao)
          stateCategories.push(element.descricao)
          stateSeries[0].data.push(element.total_aberto)
          stateSeries[1].data.push(element.total_fechado)
          stateLineSeries[0].data.push(element.em_aberto)
          if (dataState.dataDBAno.length < 2) {
            stateLineSeries[0].data.push(element.em_aberto)
          }
        })
          break;
      default:
        break;
    }
    let heightBar = 0
    //mapeia os problemas
    dataState.dataDB.grupos.problemas.map((element:any)=>{
      let desc = element.descricao.split('/')[1].split(':')[0]
      if (dataState.visualizacaoBars === 'Todos') {
        stateBarCategories.push(element.descricao)
        stateBarSeries.push(element.total)
        heightBar = 500
        heightBar = 2000
      }else{
        if(dataState.visualizacaoBars === desc){
          stateBarCategories.push(element.descricao)
          stateBarSeries.push(element.total)
          heightBar += 30
        }
      }
    })
    //atualiza a altura da barra 1
    useEffect(()=>{
      if (heightBar>400) {
        
        setHeightBar1(heightBar)
      }else{
        setHeightBar1(100+heightBar)}
    },[heightBar])

   //mapeia os operadores
    const auxObjectChart:any = []
    let heightBar2aux = 0
    dataState.dataDB.grupos.operadores.map((element:any)=>{
      let desc = Object.keys(element.departamentos).filter(key=>dataState.visualizacaoBars == key)[0]
      if (dataState.visualizacaoBars === 'Todos') {
        stateBar2Categories.push(element.descricao)
        stateBar2Series.push(parseInt(element.total))
        heightBar2aux = 500
        heightBar2aux = 2000 
      }else{
        if(dataState.visualizacaoBars == desc){
          auxObjectChart.push({label:element.descricao,value:parseInt(element.departamentos[desc])})
          heightBar2aux += 30
        }
      }
    })
    //ordena os operadores caso o tipo for diferente de todos
    if(dataState.visualizacaoBars != "Todos"){
      auxObjectChart.sort(function (a:any,b:any) { return b.value-a.value; })
      auxObjectChart.map((element:any)=>{
        stateBar2Categories.push(element.label)
        stateBar2Series.push(element.value)
      })
    }
    //atualiza altura da barra2
    useEffect(()=>{
      if (heightBar2aux>400) {
        
        setHeightBar2(heightBar2aux)
      }else{
        setHeightBar2(100+heightBar2aux)}
    },[heightBar2aux])

       

    //grafico de barras
    const state:ChartType = {
      series: stateSeries,
      options: {
        legend: {
          position: 'top',
          offsetY: 20,
          labels: {
            colors: '#FFF',
            useSeriesColors: false
          },
        },
        chart: {
          height: 350,
          type: 'bar',
          events: {
            dataPointSelection: function (event:any, chartContext:any, config:any) {
              handleOpenModal('period',config.w.config.xaxis.categories[config.dataPointIndex])
            }
          }
        },
        plotOptions: { // valor de cada ponto
          bar: {
            borderRadius: 10,
            dataLabels: {
              position: 'top', // top, center, bottom
            },
          }
        },
        dataLabels: {
          enabled: true,
          formatter: function (val:any) {
            return val ;
          },
          offsetY: -20,
          style: {
            fontSize: '12px',
            colors: ['#FFF']
          }
        },
        xaxis: {
          labels: {
            rotate: -45,
            rotateAlways: true,
            style: {
              colors: '#a7a7a7',
            }
          },
          categories: stateCategories,
          position: 'bottom',
          axisBorder: {
            show: true
          },
          axisTicks: {
            show: false
          },
          crosshairs: {
            fill: {
              type: 'gradient',
              gradient: {
                colorFrom: '#FFF',
                colorTo: '#FFF',
                stops: [0, 100],
                opacityFrom: 0.4,
                opacityTo: 0.5,
              }
            }
          },
          tooltip: {
            enabled: true,
          }
        },
        grid:{
          show:false,
        },
        yaxis: {
          axisBorder: {
            show: false
          },
          axisTicks: {
            show: false,
          },
          labels: {
            show: false,
            formatter: function (val:any) {
              return val ;
            }
          }
  
        },
        title: {
          text: 'Chamados ao longo do tempo',
          floating: true,
          offsetY: -5,
          align: 'left',
          style: {
            color: '#FFF',
            fontSize: 18,
            fontFamily: 'Roboto',
            fontWeight: 400
          }
        }
      },
      tooltip:{}
    };
  //grafico de linha
    const stateLine:ChartType = {
        tooltip:{},
        series: stateLineSeries,
        options: {
            chart: {
              height: 350,
              type: 'area',
              zoom: {
                  enabled: true
              },
            },
            dataLabels: {
              enabled: true
            },
            stroke: {
            curve: 'straight'
            },
            title: {
            text: 'Chamados em aberto ao longo do tempo',
            align: 'left',
            style: {
                color: '#FFF',
                fontSize: 18,
                fontFamily: 'Roboto',
                fontWeight: 400
            }
            },
            grid: {
            show:false,
            row: {
                colors: ['transparent'], // takes an array which will be repeated on columns
                opacity: 0.1
            },
            },
            xaxis: {
              type: 'category',
              categories:stateLineCategories,
              tickPlacement: 'between',
              labels: {
                  rotate: -45,
                  style: {
                  colors: '#a7a7a7',
                  }
              },
            },
            yaxis: {
            labels: {
                style: {
                colors: '#a7a7a7',
                }
            },
            }
        },
    };
    //grafico de problemas
    const stateBar:ChartType = {
        tooltip:{},
        series: [{
            data: stateBarSeries
        }],
        options: {
            chart: {
              type: 'bar',
              width: 10,
              events: {
                dataPointSelection: function (event:any, chartContext:any, config:any) {
                  handleOpenModal('problema',config.w.config.xaxis.categories[config.dataPointIndex])
                }
              }
            },
            plotOptions: {
            bar: {
                borderRadius: 4,
                horizontal: true,
            }
            },
            title: {
            text: 'Total atendimentos por Problema',
            align: 'left',
            style: {
                color: '#FFF',
                fontSize: 18,
                fontFamily: 'Roboto',
                fontWeight: 400
            }
            },
            dataLabels: {
              enabled: true,
              position: 'top', // top, center, bottom
            },
            xaxis: {
              show: true,
              position:'top',
              categories: stateBarCategories ,
              labels: {
                style: {
                  colors: '#a7a7a7',
                },
              },
            },
            yaxis: {
              labels: {
                  show: true,
                  align: 'right',
                  minWidth: 0,
                  maxWidth: 250,
                  style: {
                    fontSize:'14px',
                    colors: '#FFF',
                  },
              },
            },
            grid: {
              show:false,
            }
        },
    };
    //grafico de operadores
    const stateBar2:ChartType = {
        tooltip:{},
        series: [{
          data: stateBar2Series
        }],
        options: {
          chart: {
            type: 'bar',
            events: {
              dataPointSelection: function (event:any, chartContext:any, config:any) {
                handleOpenModal('operador',config.w.config.xaxis.categories[config.dataPointIndex])
              },
            }
          },
          plotOptions: {
            bar: {
              borderRadius: 4,
              horizontal: true,
            }
          },
          title: {
            text: 'Total atendimento por Operador',
            align: 'left',
            style: {
              color: '#FFF',
              fontSize: 18,
              fontFamily: 'Roboto',
              fontWeight: 400
            }
          },
          dataLabels: {
            enabled: true,
            position: 'top', // top, center, bottom
           
          },
          xaxis: {
            show: true,
            position:'top',
            categories:stateBar2Categories,
            labels: {
              rotate: -45,
              style: {
                colors: '#a7a7a7',
              },
            },
          },
          yaxis: {
            labels: {
              show: true,
              align: 'right',
              minWidth: 0,
              maxWidth: 250,
              style: {
                fontSize:'14px',
                colors: '#FFF',
              }
            },
          },
          grid: {
            show:false,
          }
        },
    };
//grafico do total de encerramento
    const stateRadialBar:ChartType = {
        tooltip:{},
        series: [(100 - porcBarra).toFixed(0)],
        options: {
          chart: {
            type: 'radialBar',
            events: {
              dataPointSelection: function (event:any, chartContext:any, config:any) {
              }
            }
          },
          plotOptions: {
            radialBar: {
              hollow: {
                size: '60%',
              },
              dataLabels: {
                show: true,
                name: {
                  show: true,
                  fontSize: '20px',
                  fontFamily: 'Roboto',
                  fontWeight: 600,
                },
                value: {
                  show: true,
                  fontSize: '16px',
                  fontFamily: 'Roboto',
                  fontWeight: 600,
                  color: '#FFF',
                  formatter: function (val:any) {
                    return val + '%'
                  }
                },
              }
            },
          },
          grid:{
            show:false,
          },
          labels: ['Fechados'],
        },
    }
    //adiciona o evento de click nas labels
    useEffect(()=>{
      const labels = document.querySelectorAll('.apexcharts-text.apexcharts-yaxis-label')
      labels.forEach(item => {
        item.addEventListener('click', (event:any)=> {
          if(event.target.innerHTML.split(':').length > 1){
            handleOpenModal('problema',event.target.innerHTML)
          }else{
            handleOpenModal('operador',event.target.innerHTML)
          }
        })
      })
    })
    return(
        
        <div id='body' className={clsx(classes.content, { [classes.contentShift]: drawerState.open, })} >
            <div id='charts' style={{ display: 'flex', flexWrap: 'wrap', width: '100%', justifyContent: 'space-evenly' }}>
              <div id="chart" className={classes.cardMainBar}>
                  <Chart options={state.options} series={state.series} type="bar" height={300}  width={'100%'} />
              </div>
              <div id="chart" className={classes.card} style={{overflow:'auto'}} >
                <div style={{marginTop:'-5px'}}>
                  <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} style={{color:'#FFF'}}>
                    {dataState.visualizacaoBars}
                  </Button>
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={()=>handleClose(dataState.visualizacaoBars)}
                  >
                    <MenuItem onClick={()=> {handleClose('Todos')}}>Todos</MenuItem>
                    <MenuItem onClick={()=> {handleClose('ADM')}}>ADM</MenuItem>
                    <MenuItem onClick={()=> {handleClose('ALMOX')}}>ALMOX</MenuItem>
                    <MenuItem onClick={()=> {handleClose('ATD')}}>ATD</MenuItem>
                    <MenuItem onClick={()=> {handleClose('BELLUNO')}}>BELLUNO</MenuItem>
                    <MenuItem onClick={()=> {handleClose('BOT')}}>BOT</MenuItem>
                    <MenuItem onClick={()=> {handleClose('CANC')}}>CANC</MenuItem>
                    <MenuItem onClick={()=> {handleClose('COB')}}>COB</MenuItem>
                    <MenuItem onClick={()=> {handleClose('COM')}}>COM</MenuItem>
                    <MenuItem onClick={()=> {handleClose('FAT')}}>FAT</MenuItem>
                    <MenuItem onClick={()=> {handleClose('FIN')}}>FIN</MenuItem>
                    <MenuItem onClick={()=> {handleClose('GRL')}}>GRL</MenuItem>
                    <MenuItem onClick={()=> {handleClose('LVT')}}>LVT</MenuItem>
                    <MenuItem onClick={()=> {handleClose('REL')}}>REL</MenuItem>
                    <MenuItem onClick={()=> {handleClose('Recolha')}}>Recolha</MenuItem>
                    <MenuItem onClick={()=> {handleClose('SUP')}}>SUP</MenuItem>
                  </Menu>
                </div>
                  <Chart options={stateBar.options} series={stateBar.series} type="bar" height={heightBar1} width={'100%'} />
              </div>
                              
              <div id="chart" className={classes.cardSecondCard}>
                  <div id="chart" className={classes.cardMainBar} style={{margin:'auto'}} >
                    <Chart options={stateLine.options} series={stateLine.series} type="area" height={290} width={'100%'} />
                  </div>
                  <div id="chart" className={classes.cardRadial} >
                    <Chart options={stateRadialBar.options} series={stateRadialBar.series} type="radialBar" height={'100%'} />
                  </div>
              </div>
              <div id="chart" className={classes.card} style={{overflow:'auto'}}>
                  
                  <Chart options={stateBar2.options} series={stateBar2.series} type="bar" height={heightBar2} width={'100%'} />
              </div>
            </div>
            <Modal
              aria-labelledby="transition-modal-title"
              aria-describedby="transition-modal-description"
              className={classes.modal}
              open={dataState.isModalOpen}
              onClose={handleCloseModal}
              closeAfterTransition
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 500,
              }}
            >
              <Fade in={dataState.isModalOpen}>
                <div className={classes.paper}>
                  <h1>{dataState.modalLabel}</h1>
                  <DataGrid
                    rows={dataState.currentTableModalData}
                    columns={dataState.currentTableModalHeader}
                    disableSelectionOnClick
                    density='compact'
                    style={{height:'90%'}}
                    components={{
                      Toolbar: GridToolbar,
                    }}
                  />
                </div>
              </Fade>
            </Modal>
        </div>
    )
}