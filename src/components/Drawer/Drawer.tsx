import React, { useState, useEffect } from 'react'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';
import { useAppSelector as useSelector, useAppDispatch as useDispatch } from '../../hooks/hooks';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
// import SwapHorizIcon from '@material-ui/icons/SwapHoriz';
import { InputLabel, Input, Button } from '@material-ui/core';
import axios from 'axios'
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import img from '../../media/LOGO-NOVO.png';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';


const drawerWidth = 230;

const useStyles = makeStyles((theme?: any) => ({
    drawer: {
        backgroundColor: '#545454',
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        backgroundColor: '#545454',
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    imgLogo: {
        width: '70%',
        height: 'auto'
    }
}));



export default function DrawerComponent() {

    const [openMenu, setOpenMenu] = useState(null);
    const [integracoes, setIntegracoes] = useState([]);
    const handleClickIntegracao = (event: any) => {
        setOpenMenu(event.currentTarget);
    };

    const switchIntegracao = (id: number, nome: string) => {
        setOpenMenu(null);
        dispatch({ type: 'filter/setIntegracaoNome', payload: nome })
        dispatch({ type: 'filter/setIntegracao', payload: id })
    }

    //troca a visualização de dia/mes/ano
    const handleAlignment = (event: any, newAlignment: any) => {
        if (newAlignment != null) {
            dispatch({ type: 'filter/setVisualizacao', payload: newAlignment })
        }
    };

    const [tipo, setTipo] = useState('')
    const dispatch = useDispatch();
    const classes = useStyles();
    const drawerState = useSelector(state => state.drawer)
    // const [filterChoice, setFilterChoice] = useState(true)
    const filterChoice = true
    const filterState = useSelector(state => state.filter)
    const handleDrawerClose = () => {
        dispatch({ type: 'drawer/closeDrawer', payload: false })
    };
    //executa ao atualizar o filtro de periodo
    const submitFilterInputs = () => {
        console.log(filterState)
        let desde = new Date(filterState.desde)
        let ate = new Date(filterState.ate)
        let integracao = filterState.integracao;
        let num = ate.getTime() - desde.getTime()
        let dias: number = num / (1000 * 3600 * 24)
        if (dias <= 31) {
            dispatch({ type: 'filter/setVisualizacao', payload: 'dia' })
            setTipo('dia')
        }
        else if (dias <= 365 * 2) {
            dispatch({ type: 'filter/setVisualizacao', payload: 'mes' })
            setTipo('mes')
        }
        else {
            dispatch({ type: 'filter/setVisualizacao', payload: 'ano' })
            setTipo('ano')
        }

        dispatch({ type: 'dataDB/setLoading', payload: true })
        let url = process.env.REACT_APP_API_HOST + 'api/v1/flyBI/getPeriod?start=' + filterState.desde + '&end=' + filterState.ate;
        if (integracao > 0) {
            url = url + '&integracaoId=' + integracao;
        }
        console.log(url);
        axios.get(url)
            .then((res) => {
                let data = JSON.parse(JSON.stringify(res.data))
                console.log(data)
                if (data.data.atual.total_atendimentos) {
                    dispatch({ type: 'dataDB/setDataDB', payload: data.data.atual })
                    dispatch({
                        type: 'statusChart/setStatusChartOnScreen', payload: [new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(data.data.atual.total_atendimentos),
                        new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(data.data.atual.total_fechados),
                        new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(data.data.atual.media_fechados_por_dia),
                        new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(data.data.atual.total_abertos)]
                    })
                    if (data.data.passado.grupos) {
                        data.data.passado.grupos.meses.map((element: any) => {
                            dispatch({ type: 'statusChart/setFirstChartDataOnScreen', payload: element.total_aberto })
                            dispatch({ type: 'statusChart/setSecondChartDataOnScreen', payload: element.total_fechado })
                            dispatch({ type: 'statusChart/setThirdChartDataOnScreen', payload: element.media })
                        })
                    }
                } else {
                    dispatch({
                        type: 'dataDB/setDataDB',
                        payload: {
                            grupos: {
                                dias: [],
                                meses: [],
                                anos: [],
                                operadores: [],
                                problemas: [],
                            },
                            media_fechados_por_dia: 0,
                            total_abertos: 0,
                            total_atendimentos: 0,
                            total_fechados: 0,
                        }
                    })
                    dispatch({
                        type: 'statusChart/setStatusChartOnScreen', payload: [new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(0),
                        new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(0),
                        new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(0),
                        new Intl.NumberFormat('pt-BR', { style: 'decimal' }).format(0)]
                    })
                    data.data.passado.grupos.meses.map((element: any) => {
                        dispatch({ type: 'statusChart/setFirstChartDataOnScreen', payload: '' })
                        dispatch({ type: 'statusChart/setSecondChartDataOnScreen', payload: '' })
                        dispatch({ type: 'statusChart/setThirdChartDataOnScreen', payload: '' })
                    })
                }
                dispatch({ type: 'dataDB/setLoading', payload: false })
            })
    }


    const changeDesde = (event: any) => {
        dispatch({ type: 'filter/setDesde', payload: event.target.value })
    }

    const changeAte = (event: any) => {
        dispatch({ type: 'filter/setAte', payload: event.target.value })
    }

    useEffect(() => {
        let url = process.env.REACT_APP_API_HOST + 'api/v1/flyBI/getintegracoes';
        axios.get(url)
            .then((res) => {
                let integracoesResult = res.data.data;
                setIntegracoes(integracoesResult);
            });
        let desde = new Date(filterState.desde)
        let ate = new Date(filterState.ate)
        let num = ate.getTime() - desde.getTime()
        let dias: number = num / (1000 * 3600 * 24)
        if (dias <= 31) {
            dispatch({ type: 'filter/setVisualizacao', payload: 'dia' })
            setTipo('dia')
        }
        else if (dias <= 365 * 2) {
            dispatch({ type: 'filter/setVisualizacao', payload: 'mes' })
            setTipo('mes')
        }
        else {
            dispatch({ type: 'filter/setVisualizacao', payload: 'ano' })
            setTipo('ano')
        }
    }, [])

    return (
        <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor="left"
            open={drawerState.open}
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <div className={classes.drawerHeader}>
                <img src={img} alt="FLY-LVT" className={classes.imgLogo} />
                <IconButton onClick={handleDrawerClose}>
                    <MenuIcon />
                </IconButton>
            </div>
            {/* <Divider />
            <div style={{ display: 'flex', flexDirection: 'row', padding: 2 }} >
            <p style={{ marginLeft: '50%', fontSize: '16px', color: '#FFF' }}>Filtro</p>
            <IconButton style={{ marginLeft: 5 }}>
                <SwapHorizIcon style={{ color: 'rgba(0, 143, 251, 1)' }} onClick={()=>{setFilterChoice(!filterChoice)}} />
            </IconButton> */}
            {/* </div> */}
            <Divider />
            {filterChoice === true ? (
                <div>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClickIntegracao} style={{ color: '#FFF' }}>
                            {filterState.integracaoNome}
                        </Button>
                        <Menu
                            id="simple-menu"
                            anchorEl={openMenu}
                            keepMounted
                            open={Boolean(openMenu)}
                            onClose={() => setOpenMenu(null)}
                        >
                            <MenuItem onClick={() => { switchIntegracao(0, "Todos os Sistemas") }}>Todos os Sistemas</MenuItem>
                            {integracoes.map((integracao: any) => {
                                return(<MenuItem onClick={() => { switchIntegracao(integracao.id, integracao.descricao) }}>{integracao.descricao}</MenuItem>)
                            })}
                        </Menu>
                    </div>
                    <div style={{ margin: 15, marginTop: 50 }}>
                        <InputLabel style={{ color: '#FFF', margin: 10 }}>
                            Visualização:
                        </InputLabel>
                        {
                            tipo === 'dia' ? (
                                <ToggleButtonGroup
                                    value={filterState.visualizacao}
                                    exclusive
                                    onChange={handleAlignment}
                                    aria-label="text alignment"
                                >
                                    <ToggleButton value="dia" aria-label="left aligned" >
                                        <p style={{ color: '#FFF' }}>Dia</p>
                                    </ToggleButton>
                                    <ToggleButton value="semana" aria-label="left aligned" disabled>
                                        <p style={{ color: '#666' }}>Sem</p>
                                    </ToggleButton>
                                    <ToggleButton value="mes" aria-label="left aligned">
                                        <p style={{ color: '#FFF' }}>Mês</p>
                                    </ToggleButton>
                                    <ToggleButton value="ano" aria-label="left aligned">
                                        <p style={{ color: '#FFF' }}>Ano</p>
                                    </ToggleButton>
                                </ToggleButtonGroup>
                            ) : tipo === 'mes' ? (
                                <ToggleButtonGroup
                                    value={filterState.visualizacao}
                                    exclusive
                                    onChange={handleAlignment}
                                    aria-label="text alignment"
                                >
                                    <ToggleButton value="dia" aria-label="left aligned" disabled>
                                        <p style={{ color: '#666' }}>Dia</p>
                                    </ToggleButton>
                                    <ToggleButton value="semana" aria-label="left aligned" disabled>
                                        <p style={{ color: '#666' }}>Sem</p>
                                    </ToggleButton>
                                    <ToggleButton value="mes" aria-label="left aligned" >
                                        <p style={{ color: '#FFF' }}>Mês</p>
                                    </ToggleButton>
                                    <ToggleButton value="ano" aria-label="left aligned">
                                        <p style={{ color: '#FFF' }}>Ano</p>
                                    </ToggleButton>
                                </ToggleButtonGroup>

                            ) : (
                                <ToggleButtonGroup
                                    value={filterState.visualizacao}
                                    exclusive
                                    onChange={handleAlignment}
                                    aria-label="text alignment"
                                >
                                    <ToggleButton value="dia" aria-label="left aligned" disabled>
                                        <p style={{ color: '#666' }}>Dia</p>
                                    </ToggleButton>
                                    <ToggleButton value="semana" aria-label="left aligned" disabled >
                                        <p style={{ color: '#666' }}>Sem</p>
                                    </ToggleButton>
                                    <ToggleButton value="mes" aria-label="left aligned" disabled>
                                        <p style={{ color: '#666' }}>Mês</p>
                                    </ToggleButton>
                                    <ToggleButton value="ano" aria-label="left aligned">
                                        <p style={{ color: '#FFF' }}>Ano</p>
                                    </ToggleButton>
                                </ToggleButtonGroup>
                            )
                        }
                    </div>
                    <div style={{ margin: 15, marginTop: 50 }}>
                        <InputLabel style={{ color: '#FFF' }}>
                            Desde (dd/mm/aaaa):
                        </InputLabel>
                        <Input type='date' value={filterState.desde} onChange={changeDesde} />
                    </div>
                    <div style={{ margin: 15, marginTop: 50 }}>
                        <InputLabel style={{ color: '#FFF' }}>
                            Até (dd/mm/aaaa):
                        </InputLabel>
                        <Input type='date' value={filterState.ate} onChange={changeAte} />
                    </div>
                    <Button style={{ margin: 45, marginTop: 10, backgroundColor: 'rgba(0, 143, 251, 1)' }} onClick={() => { submitFilterInputs() }}>
                        <InputLabel>
                            Mostrar
                        </InputLabel>
                    </Button>
                </div>
            ) : (
                <div>
                    <Button style={{ margin: 45, marginTop: 0 }}>
                        <InputLabel>
                            Mostrar
                        </InputLabel>
                    </Button>
                </div>
            )}

        </Drawer>
    )
}

